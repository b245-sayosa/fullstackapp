import { Row , Col, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Banner(){
	
	return(
		<Row className='mt-5'>
			<Col className='text-center pt-1'>
				<h1>Everything Shop</h1>
				<p className='pt-1'>Everything For Everyone
				</p>
				<Button as = {Link} to = '/products' className='pt-1'>HOW ARE YOU TO FIND OUT!</Button>

			</Col>
		</Row>
			
		
		)
}
