import React from "react"
import { Button, Col, Row } from "react-bootstrap"
import Card from 'react-bootstrap/Card';

export default function Highlights () {
	return (
	<Row className="mt-5">
		<Col className="col-md-4 col-10 mx-auto m-md-0 m-1">

		<Card className="cardHighlight" style={{ width: '18rem' }}>
		<Card.Img variant="top" src="https://res.cloudinary.com/dgzasex9s/image/upload/v1677235323/Ebrithing_1_u6umzb.jpg" />
		<Card.Body>
		  <Card.Title></Card.Title>
		  <Card.Text>
		    We got EVERYTHING for you!
		  </Card.Text>
		  <Button variant="primary">BUY NAPO!</Button>
		</Card.Body>
	  </Card>
	  </Col>


	  <Col className="col-md-4 col-10 mx-auto m-md-0 m-1">
	    <Card className="cardHighlight" style={{ width: '18rem' }}>
      <Card.Img variant="top" src="https://res.cloudinary.com/dgzasex9s/image/upload/v1677235317/Ebrithing_2_dfyjej.jpg" />
      <Card.Body>
        <Card.Title>SHOP NOW PAY LATURR!</Card.Title>
        <Card.Text>
          EVERYTHINGs Gonna Be ALRIGHT!
        </Card.Text>
        <Button variant="primary">U SURE U WANNA BUY??</Button>
      </Card.Body>
    </Card>
	</Col>

	<Col className="col-md-4 col-10 mx-auto m-md-0 m-1">
	  <Card className="cardHighlight" style={{ width: '18rem' }}>
      <Card.Img variant="top" src="https://res.cloudinary.com/dgzasex9s/image/upload/v1677235311/Ebrithing_3_zpyb1z.jpg" />
      <Card.Body>
        <Card.Title>HOW ARE YOU</Card.Title>
        <Card.Text>
          TO
        </Card.Text>
        <Button variant="primary">FIND OUT</Button>
      </Card.Body>
    </Card>
	</Col>

</Row>
	)
}