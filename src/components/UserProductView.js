import {useState, useEffect} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams} from 'react-router-dom';

export default function UserProductView(){

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');

	//get the parameter from the url using the useParams

	const {productId} = useParams();

	useEffect(()=>{
		console.log(productId);

		fetch(`http://localhost:4000/product/${productId}`)
		.then(result=> result.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})



	}, [productId]);

	const order = (id) => {
		//we are going to fetch the route of the login 
		fetch(`http://localhost:4000/order/${id}`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(data => {
			console.log(data);
		})
	}

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<Card.Subtitle>Available</Card.Subtitle>
							<Card.Text>Anytime</Card.Text>
							<Button variant="primary" onClick = {()=> order(productId)}>Buy</Button>
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
		)
}
