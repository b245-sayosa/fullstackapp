import React, { Fragment, useContext } from 'react'
import { Container, Nav, Navbar } from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom'
import UserContext from '../UserContext';

export default function AppNavBar () {
//if you want to get the item in our localStorage you can use the .getItem("property");
console.log(localStorage.getItem("email"));

//let us create a new state for the user;
const {user} = useContext(UserContext);


  return (

    <Navbar bg="light" expand="lg" className='sticky-top'>
    <Container>
      <Navbar.Brand as = {Link} to = "/">EVERYTHINGShop</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ms-auto">
        <Nav.Link as = {NavLink} to = "/">Home</Nav.Link>
		            <Nav.Link as = {NavLink} to = "/products">Products</Nav.Link>

		            {/*conditional rendering*/}
		            {
		            	user ? 
		            	<Nav.Link as = {NavLink} to = "/logout">Logout</Nav.Link>
		            	:
		            	<Fragment>
		            		<Nav.Link as = {NavLink} to = "/register">Register</Nav.Link>

		            		<Nav.Link as = {NavLink} to = "/login">Login</Nav.Link>
		            	</Fragment>

		            }

          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
	
  )
}

