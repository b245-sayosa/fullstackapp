import {Row, Col, Button, Card} from 'react-bootstrap';
import {useContext, useEffect, useState} from 'react';
import UserContext from '../UserContext';
import { Link } from 'react-router-dom';


export default function ProducCard({productProp}){

	

	const {name, description, price} = productProp;
	

	const [orders, setOrders] = useState(0);
	const [stocks, setStocks] = useState(10)
	const [isDisabled, setIsDisabled] = useState(false)
	const {user} = useContext(UserContext);



	

	function order(){
		if( stocks > 1 && orders < 9){
		setOrders(orders+1);
		setStocks(stocks-1)
	}else{
		alert("congrats")
	}
}

useEffect(()=>{
	if(stocks === 0){
		setIsDisabled(true)
	}
}, [stocks])


	return(
		<Row className = "mt-5">
			<Col>
				<Card>
				    <Card.Body>
				        <Card.Title>{name}</Card.Title>
				        <Card.Subtitle>Description:</Card.Subtitle>
				        <Card.Text>{description}</Card.Text>
				        <Card.Subtitle>Price:</Card.Subtitle>
				        <Card.Text>PhP {price}</Card.Text>

				        <Card.Subtitle>Orders:</Card.Subtitle>
				        <Card.Text>{orders}</Card.Text>

				        <Card.Subtitle>Available Stocks:</Card.Subtitle>
				        <Card.Text>{stocks}</Card.Text>

				        
				        {
				        	user ?
				        	<Button as = {Link} to = {`/product/${_id}`} disabled = {isDisabled}>see more details</Button>
				        	:
				        	<Button as = {Link} to ="/login">Login</Button>
				        }

				    </Card.Body>
				</Card>

			</Col>
		</Row>
	)
}
