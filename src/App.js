

import {UserProvider} from './UserContext.js';
import AppNavbar from './components/AppNavBar';
// import ProductCard from './components/ProductCard';
import { BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import PageNotFound from './pages/PageNotFound';
import { useState } from 'react';
import UserProductView from './components/UserProductView.js';

function App() {

  const [user, setUser] = useState(localStorage.getItem('email'));

  const unSetUser = ()=>{
    localStorage.clear();
  }



  //Storing information in a context object is done by providing the information using the correspoding "Provider" and passing information thru the prop value;
  //all infromation/data provided to the Provider component can be access later on from the context object properties
    
  return (
    <UserProvider value ={{user, setUser, unSetUser}}>
        <Router>
            <AppNavbar/>
           <Routes>
             <Route path="/" element = {<Home/>}/>
             <Route path="/products" element = {<Products/>}/>
             <Route path="/login" element = {<Login/>}/>
             <Route path="/register" element = {<Register/>}/>
             <Route path="*" element = {<PageNotFound/>}/>
             <Route path="/logout" element = {<Logout/>}/>
             <Route path="/product/:productId" element = {<UserProductView/>}/>
            
          </Routes>
        </Router>
    </UserProvider>

  );
}

export default App;
