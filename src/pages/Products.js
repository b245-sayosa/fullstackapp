
import ProductCard from '../components/ProductCard';
import {Fragment, useEffect, useState} from 'react';

export default function Products(){
	// console.log(productData);

	// const products = productData.map( product =>{

	// 	return( <ProductCard key = {product.id} productProp = {product}/>

	// 		)
	// })

	const [product, setProduct] = useState([]);

	useEffect(()=>{
		//fetch all courses
		fetch("http://localhost:4000/product/all")
		.then(result => result.json())
		.then(data => {
			console.log(data);
			//to change the value of our courses so we have to use the setCourses
			setProduct(data.map(product =>{
				return(<ProductCard key = {product._id} productProp = {product}/>)
			}))
		})

	}, []);


	return(
		<Fragment>
		<h1 className='text-center mt-3'>Products</h1>
			{product}
		</Fragment>
		)

}